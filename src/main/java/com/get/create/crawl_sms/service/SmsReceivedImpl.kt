package com.get.create.crawl_sms.service


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.util.Log
import androidx.annotation.RequiresApi
import com.get.create.crawl_sms.models.ShortMessageServices
import java.lang.Exception

/**
*  crawl tin nhan moi ve
* */

class SmsReceivedImpl:  BroadcastReceiver() {
    companion object {
        // call nguoc getNewSms de lay object roi truyen cho list
        lateinit var getNewSms: (shortMessageServices: ShortMessageServices) -> Unit
    }
    @RequiresApi(Build.VERSION_CODES.M)
    // Nhan tin nham moi nhat
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            val data = intent?.extras
            if (data != null) {
                try {
                    val pdus = data["pdus"] as Array<*>?
                    for (i in pdus!!.indices) {
                        val smsMessage: SmsMessage = SmsMessage.createFromPdu(
                                pdus[i] as ByteArray?, data.getString("format")
                        )
                        val title = smsMessage.displayOriginatingAddress.toString()
                        val createTime = smsMessage.timestampMillis.toString()
                        val body = smsMessage.messageBody.toString()
                        if (context != null) {
                            val shortMessageServices = ShortMessageServices(createTime, title, body)
                            getNewSms(shortMessageServices)
                        }
                    }
                } catch (e: Exception) {
                    Log.d("check", "error $e")
                }
            }

        }
    }
}