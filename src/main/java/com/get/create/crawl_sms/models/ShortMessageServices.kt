package com.get.create.crawl_sms.models

data class ShortMessageServices(
    val time :String?,
    val title : String?,
    val content: String?
)
