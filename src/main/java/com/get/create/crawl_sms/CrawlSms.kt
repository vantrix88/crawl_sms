package com.get.create.crawl_sms

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.get.create.crawl_sms.models.ShortMessageServices
import com.get.create.crawl_sms.service.SmsReceivedImpl


class CrawlSms(private val activity: Activity,private val context: Context) {
    private val listSms : MutableList<ShortMessageServices> = arrayListOf()
    // lay all list va duoc update
    lateinit var updateNewList : (list : MutableList<ShortMessageServices>) -> Unit
    lateinit var updateNewItemSms : (shortMessageServices: ShortMessageServices) -> Unit

    init {
        requestSmsPermission()
        crawlSmsHistory()
        SmsReceivedImpl.getNewSms ={
            listSms.add(it)
            updateNewList(listSms)

            updateNewItemSms(it)
        }
    }

    private fun crawlSmsHistory(){

        val uriSms : Uri = Uri.parse("content://sms/inbox")

        val cur : Cursor? = activity.contentResolver.query(uriSms, null, null, null, null)
        while (cur != null && cur.moveToNext()){
            val address = cur.getString(cur.getColumnIndex("address"))
            val body = cur.getString(cur.getColumnIndexOrThrow("body"))
            val date: String = cur.getString(cur.getColumnIndex("date"))

            listSms.add(ShortMessageServices(date,address,body))
        }
        cur?.close()
    }

    private fun requestSmsPermission() {
        val permission = Manifest.permission.READ_SMS
        val grant = ContextCompat.checkSelfPermission(context, permission)
        if (grant != PackageManager.PERMISSION_GRANTED) {
            val permissionList = arrayOfNulls<String>(1)
            permissionList[0] = permission
            ActivityCompat.requestPermissions(activity, permissionList, 123)
        }
    }

    // add function to activity override fun onRequestPermissionsResult
    fun checkPermissionIsAgree(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_SMS), 123)
        }
    }

    fun getCurrentListSms() : MutableList<ShortMessageServices> = listSms
}